from db_program import Students
import pytest

db=None
def setup_module(module):
    print("------Setup----------")
    global db
    db=Students()
    db.connect('data.json')


def teardown_module(module):
    print("----------Teardown-----------")
    db.close()


def test_akshay_data():
    data=db.get_data('Akshay')
    assert data['id']==1
    assert data['name']=='Akshay'
    assert data['result']=='pass'


def test_abc_data():
    data=db.get_data('ABC')
    assert data['id']==2
    assert data['name']=='ABC'
    assert data['result']=='fail'