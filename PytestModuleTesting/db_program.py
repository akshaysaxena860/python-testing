import json

class Students:
    def __init__(self):
        self.__data=None

    def connect(self,data_file):
        with open(data_file) as json_file:
            self.__data=json.load(json_file)

    def get_data(self,name):
        for std in self.__data['students']:
            if std['name']==name:
                return std

    def close(self):
        print("Close Connection")