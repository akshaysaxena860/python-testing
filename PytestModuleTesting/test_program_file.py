import program_file as pg
import pytest

@pytest.mark.parametrize('num1,num2,res',[
    (1,2,3),
    ("Hello"," World","Hello World"),
    (4.5,5.5,10.0)
])
def test_add_func(num1,num2,res):
    assert pg.add_numbers(num1,num2)==res


def test_prod_func():
    assert pg.prod_numbers(12,3)==36
    assert pg.prod_numbers(10,2)==20
    assert pg.prod_numbers(1221321012,12921201)==(1221321012*12921201)